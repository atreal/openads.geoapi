"""Database mapping definitions."""

from sqlalchemy import Table, MetaData, Column, Sequence, Integer, String, Boolean, Text, join
from sqlalchemy.orm import column_property, mapper
from geoalchemy2 import Geometry

from entities import EntityParcelle, EntityContrainte, EntityDossier, EntityDossierESRI


def map_entities():
    """Map the entities classes with their matching tables."""

    # Database metadata
    db_metadata = MetaData()


    # Database table 'parcelles'
    db_table_parcelles = Table(
        'parcelles',
        db_metadata,
        Column('objectid', Integer, primary_key=True),
        Column('ccocom', String(3)),
        Column('ccodep', String(3)),
        Column('ccodir', String(3)),
        Column('ccopre', String(3)),
        Column('ccosec', String(3)), # only 2 char in openADS, one letter => first one must be a '0'
        Column('dnupla', String(4)),
        Column('ident', String(9)),
        Column('ndeb', String(4)),   # numéro de voie
        Column('sdeb', String(3)),   # indice de voie (bis, tier, etc.)
        Column('type', String(4)),   # type de voie
        Column('nom', String(30)),   # libellé voie
        Column('shape', Geometry('POLYGON'))
    )

    # Mapping between EntityParcelle and table 'parcelles'
    mapper(
        EntityParcelle, db_table_parcelles,
        properties={
            'id': db_table_parcelles.c.objectid,
            'code_commune': db_table_parcelles.c.ccocom,
            'code_departement': db_table_parcelles.c.ccodep,
            'code_direction': db_table_parcelles.c.ccodir,
            'code_quartier': db_table_parcelles.c.ccopre,
            'code_section': db_table_parcelles.c.ccosec,
            'code_numero': db_table_parcelles.c.dnupla,
            'adresse_num_voie': db_table_parcelles.c.ndeb,
            'adresse_ind_voie': db_table_parcelles.c.sdeb,
            'adresse_typ_voie': db_table_parcelles.c.type,
            'adresse_lib_voie': db_table_parcelles.c.nom,
            'geom': db_table_parcelles.c.shape,
        })

    # Database table 'contraintes'
    db_table_contraintes = Table(
        'contraintes',
        db_metadata,
        Column('objectid', Integer, primary_key=True),
        Column('id', Integer),
        Column('libelle', String(250)),     # 250 max from the openADS spec
        Column('texte', String(254)),
        Column('groupe', String(100)),      # 250 max from the openADS spec
        Column('sous_groupe', String(100)), # 250 max from the openADS spec
        Column('gid', Integer, nullable=False)
    )

    # Database table 'contraintes_loc'
    db_table_contraintes_loc = Table(
        'contraintes_loc',
        db_metadata,
        Column('objectid', Integer, primary_key=True),
        Column('id_openads', Integer),
        Column('id_contraintes_openads', Integer),
        Column('texte', String(2048), key='texte_loc'),
        Column('codeinsee', String(5)),
        Column('shape', Geometry('POLYGON'))
    )

    # Database join with tables 'contraintes' and 'contraintes_loc'
    db_join_contraintes = join(
        db_table_contraintes,
        db_table_contraintes_loc,
        db_table_contraintes.c.id == db_table_contraintes_loc.c.id_contraintes_openads
    )


    # Mapping between EntityContrainte and tables 'contraintes' and 'contraintes_loc'
    mapper(
        EntityContrainte, db_join_contraintes,
        properties={
            'id': column_property(db_table_contraintes.c.objectid,
                                  db_table_contraintes_loc.c.objectid),
            'id_openads': db_table_contraintes.c.id,
            'id_contrainte': db_table_contraintes_loc.c.id_openads,
            'text_generic': db_table_contraintes.c.texte,
            'text_specific': db_table_contraintes_loc.c.texte_loc,
            'libelle': db_table_contraintes.c.libelle,
            'groupe': db_table_contraintes.c.groupe,
            'sous_groupe': db_table_contraintes.c.sous_groupe,
            'code_insee': db_table_contraintes_loc.c.codeinsee,
            'geom': db_table_contraintes_loc.c.shape
        })


    # Database table 'dossiers_openads'
    db_table_dossiers = Table(
        'dossiers_openads',
        db_metadata,
        Column('objectid', Integer, Sequence('dossiers_openads_seq'), primary_key=True),
        Column('numero', String(20)),
        Column('codeinsee', String(5)),
        Column('parcelles', Text),
        Column('shape', Geometry('POLYGON')),
        Column('dossier_importe_geosig', Boolean)
    )


    # Mapping between EntityDossier and table 'dossiers_openads'
    mapper(
        EntityDossier, db_table_dossiers,
        properties={
            'id': db_table_dossiers.c.objectid,
            'numero': db_table_dossiers.c.numero,
            'code_insee': db_table_dossiers.c.codeinsee,
            'parcelles': db_table_dossiers.c.parcelles,
            'geom': db_table_dossiers.c.shape,
            'frozen': db_table_dossiers.c.dossier_importe_geosig,
        })


    # Database table 'dossiers_esri'
    db_table_dossiers_esri = Table(
        'dossiers_esri',
        db_metadata,
        Column('objectid', Integer, primary_key=True),
        Column('numero', String(20)),
        Column('codeinsee', String(5)),
        Column('shape', Geometry('POLYGON'))
    )

    mapper(
        EntityDossierESRI, db_table_dossiers_esri,
        properties={
            'id': db_table_dossiers_esri.c.objectid,
            'numero': db_table_dossiers_esri.c.numero,
            'code_insee': db_table_dossiers_esri.c.codeinsee,
            'geom': db_table_dossiers_esri.c.shape,
        })
