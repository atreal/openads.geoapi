"""An openADS.geoapi implementation using pyramid_openapi3.

See README.md at
https://gitlab.com/atreal/openads.geoapi/blob/master/README.md
"""

import re
import sys
import logging
import logging.config
import configparser
import json
import os
import typing as t
from dataclasses import dataclass
from wsgiref.simple_server import make_server
from openapi_core.schema.exceptions import OpenAPIError, OpenAPIMappingError
from pyramid.config import Configurator
from pyramid.httpexceptions import exception_response, HTTPException, HTTPNotFound
from pyramid.request import Request
from pyramid.view import view_config, exception_view_config, notfound_view_config
from sqlalchemy import (create_engine, Float, desc, func, or_, and_, text)
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import DatabaseError, OperationalError
from pyramid_openapi3.exceptions import RequestValidationError

from entities import EntityParcelle, EntityContrainte, EntityDossier, EntityDossierESRI
from mapping import map_entities

# Do the entities mappings
map_entities()

# Default values
OPENAPI_FILE_PATH = os.path.join(os.path.dirname(__file__), 'openapi.yaml')
CONFIG_FILE_PATH = os.path.join(os.path.dirname(__file__), 'config.ini')

# Logger (default)
LOGGER = None

# Environments
DEFAULT_ENV = None
ENVIRONMENTS = {}


def fatal_error(msg, exit_code = 2):
    """Print a message on STDERR and exit with specified code."""
    global LOGGER
    if not isinstance(msg, str):
        msg = str(msg)
    if len(msg) > 0 and msg[-1] == '\n':
        msg = msg[:-1]
    msg += "\nExiting with code '%d'" % exit_code
    try:
        LOGGER.error(msg)
    except Exception as exception:
        sys.stderr.write('\nFATAL ERROR: '.join(('FATAL ERROR: ' + msg).split('\n')))
        sys.stderr.write('\n')
    sys.exit(exit_code)


def setup_default_logger(config):
    """Setup the default logger."""
    global LOGGER
    logging.config.fileConfig(config['APPLICATION']['logging_conf_file'], disable_existing_loggers=False)
    LOGGER = logging.getLogger(config['APPLICATION']['logger_name'])


def load_environements(config):
    """Load environments configurations."""
    global ENVIRONMENTS

    # get the list of environments
    envs = config['ENVIRONMENTS']

    # define environments filenames
    db_filename = config['APPLICATION']['db_filename']
    log_filename = config['APPLICATION']['log_filename']

    # for each environment defined
    for env_name, env_dir in envs.items():
        sys.stderr.write("[DEBUG] Environment: %s => %s\n" % (env_name, env_dir))

        # add it to the environments list
        ENVIRONMENTS[env_name] = {}

        if not env_dir.startswith('/'):
            env_dir = os.path.join(os.path.dirname(__file__), env_dir)

        # ensure its directory exists
        if not os.path.isdir(env_dir):
            fatal_error("environment directory '%s' doesn't exist\n" % env_dir)

        # for each required environment file
        for env_t in [
                ('database', db_filename), ('logging', log_filename)]:
            env_file = os.path.join(env_dir, env_t[1])

            # ensure it exists
            if not os.path.isfile(env_file):
                fatal_error("%s configuration '%s' doesn't exist\n" % (env_t[0], env_file))

            # add it to the environment variables
            ENVIRONMENTS[env_name][env_t[0] + '-conf-file'] = env_file

    # setup environements
    for env_name, env in ENVIRONMENTS.items():

        # logging
        logging.config.fileConfig(env['logging-conf-file'], disable_existing_loggers=False)
        env_logger_name = 'env-' + env_name
        env['logger'] = logging.getLogger(env_logger_name)

        # Postgres connexion
        DB_config = configparser.ConfigParser()
        DB_config.read(env['database-conf-file'])
        try:
            DB_params = DB_config['DB']
        except KeyError:
            fatal_error("configuration file '%s' do not have a '%s' section" % (
                env['database-conf-file'], 'DB'))
        try:
            DB_port = DB_params['port']
        except KeyError:
            DB_port = config['APPLICATION']['db_port_default']
        try:
            # Format: dialect+driver://username:password@host:port/database
            DB_conn_string = DB_params['dialect'] + '+' + DB_params['driver']
            DB_conn_string += '://' + DB_params['username'] + ':' + DB_params['password']
            DB_conn_string += '@' + DB_params['host'] + ':' + DB_params['port']
            DB_conn_string += '/' + DB_params['database']
        except KeyError as exception:
            fatal_error("configuration file '%s' do not have required key '%s'" % (
                env['database-conf-file'], str(exception)))
        DB_engine = create_engine(DB_conn_string)
        env['db-engine'] = DB_engine
        DB_session_class = sessionmaker(bind=DB_engine)
        env['db'] = DB_session_class()


class InvalidDataSourceException(HTTPNotFound):
    pass


def get_env_for_data_source_from_request(request, log_msg_prefix = ''):
    """Return an environment configuration for a data source.
    (from request or default)"""
    global LOGGER, DEFAULT_ENV, ENVIRONMENTS

    data_source_arg = DEFAULT_ENV;
    if 'datasrc' in request.openapi_validated.parameters['query']:
        data_source_arg = request.openapi_validated.parameters['query']['datasrc']

    LOGGER.debug("%sdata source: %s", log_msg_prefix, data_source_arg)

    try:
        env = ENVIRONMENTS[data_source_arg]
        LOGGER.debug("%senv: %s", log_msg_prefix, env)
    except KeyError:
        raise InvalidDataSourceException("Invalid data source '%s'" % data_source_arg)
        #raise HTTPNotFound("Invalid data source '%s'" % data_source_arg)
    return env


def get_dossier(numero, db_session, openads=False, esri=False, logger=None):
    """Return a dossier instance, either from ESRI or openADS table or both."""

    # matching dossier instance
    matching_dossier_openads = None
    matching_dossier_esri = None

    # need to search into openADS table
    if openads:

        # search one or none dossier
        matching_dossier_openads = (db_session
                                    .query(EntityDossier)
                                    .filter_by(numero=numero)
                                    .order_by(desc(EntityDossier.id)) # pylint: disable=no-member
                                    .one_or_none())

    # need to search into ESRI table
    if esri:

        # search one or none dossier
        matching_dossier_esri = (db_session
                                 .query(EntityDossierESRI)
                                 .filter_by(numero=numero)
                                 .order_by(desc(EntityDossierESRI.id)) # pylint: disable=no-member
                                 .one_or_none())

    # none found: advertise
    if not matching_dossier_openads and not matching_dossier_esri:
        if logger:
            logger.debug(
                "No matching dossier with numero: '%s' (%s)", numero,
                'openADS or ESRI' if openads and esri else 'openADS' if openads else 'ESRI')

    # result is the matching dossier or a tuple with both results (openADS, ESRI)
    result = None
    if openads and esri:
        result = (matching_dossier_openads, matching_dossier_esri)
    elif openads:
        result = matching_dossier_openads
    elif esri:
        result = matching_dossier_esri
    return result


def get_centroide(dossier, db_session, logger=None):
    """Return the X and Y coordinate of the centroide of a geometry."""
    if logger:
        logger.debug("get_centroide() dossier: '%s'", dossier)
    # using a raw query because GeoAlchemy2 do not implement the 'ST_PointOnSurface' function
    raw_query = (text("SELECT "
                      "  ST_X(ST_PointOnSurface(shape)) AS x, "
                      "  ST_Y(ST_PointOnSurface(shape)) AS y "
                      "FROM "
                      "  dossiers_%s "
                      "WHERE "
                      "  objectid = ':dossier_id'" % (
                          'openads' if isinstance(dossier, EntityDossier) else 'esri'))
                 .columns(x=Float, y=Float)
                 .bindparams(dossier_id=dossier.id))
    query = (db_session
             .connection()
             .execute(raw_query))
    centroide = query.fetchone()
    if logger:
        logger.debug("get_centroide() centroide: '%s'", centroide)
        logger.debug("get_centroide() centroide: x: %f, y: %f", centroide[0], centroide[1])
    return centroide


@dataclass
class ParcelleAddress:
    """A 'parcelle' address."""

    numero_voie: str
    type_voie: str
    nom_voie: str
    arrondissement: str

    def __json__(self, request: Request) -> t.Dict[str, str]: # pylint: disable=unused-argument
        """JSON-renderer for this object."""
        dic = {}
        for k in ['numero_voie', 'type_voie', 'nom_voie']:
            dic[k] = getattr(self, k)
        if self.arrondissement:
            dic['arrondissement'] = self.arrondissement
        return dic


@dataclass
class ParcelleReturnNotExistType:
    """The result of the calling to /parcelles/ API when a parcelle do not exist."""
    parcelle: str
    existe: False

    def __json__(self, request: Request) -> t.Dict[str, str]: # pylint: disable=unused-argument
        return {"parcelle": self.parcelle, "existe": self.existe}


@dataclass
class ParcelleReturnExistsType(ParcelleReturnNotExistType):
    """The result of the calling to /parcelles/ API when a parcelle exists."""
    existe: True
    adresse: ParcelleAddress

    def __json__(self, request: Request) -> t.Dict[str, str]: # pylint: disable=unused-argument
        dic = super().__json__(request)
        dic["adresse"] = self.adresse
        return dic


ParcelleReturnType = t.TypeVar(
    'ParcelleReturnType',
    ParcelleReturnExistsType,
    ParcelleReturnNotExistType
)


@dataclass
class EmpriseReturnType:
    """The result of the calling to /calcul_emprise/ API."""
    statut_calcul_emprise: bool

    def __json__(self, request: Request) -> t.Dict[str, str]: # pylint: disable=unused-argument
        return {"statut_calcul_emprise": self.statut_calcul_emprise}


@dataclass
class CentroideReturnNotExistType:
    """The result of the calling to /calcul_centroide/ API when the calculation fail."""
    statut_calcul_centroide: False

    def __json__(self, request: Request) -> t.Dict[str, str]: # pylint: disable=unused-argument
        return {"statut_calcul_centroide": self.statut_calcul_centroide}


@dataclass
class CentroideReturnExistsType(CentroideReturnNotExistType):
    """The result of the calling to /calcul_centroide/ API when the calculation succeed."""
    statut_calcul_centroide: True
    coord_x: float
    coord_y: float

    def __json__(self, request: Request) -> t.Dict[str, str]: # pylint: disable=unused-argument
        dic = super().__json__(request)
        dic["x"] = self.coord_x
        dic["y"] = self.coord_y
        return dic


CentroideReturnType = t.TypeVar(
    'CentroideReturnType',
    CentroideReturnExistsType,
    CentroideReturnNotExistType
)


@dataclass
class ContrainteReturnType:
    """The result of the calling to /recup_*_contraintes/ API."""
    contrainte: str
    groupe_contrainte: str
    sous_groupe_contrainte: str
    libelle: str
    texte: str

    def __json__(self, request: Request) -> t.Dict[str, str]: # pylint: disable=unused-argument
        return {
            "contrainte": str(self.contrainte),
            "groupe_contrainte": self.groupe_contrainte,
            "sous_groupe_contrainte": self.sous_groupe_contrainte,
            "libelle": self.libelle,
            "texte": self.texte}


@view_config(route_name="verif_parcelle", renderer="json", request_method="GET", openapi=True)
def verif_parcelle(request: Request) -> t.Dict[str, t.List[ParcelleReturnType]]:
    """Check 'parcelles' existence and return their addresses."""

    # get the environment
    env = get_env_for_data_source_from_request(request, log_msg_prefix='verif_parcelle() ')

    # list of parcelles to search for
    parcelles_arg = request.openapi_validated.parameters['path']['parcelles']
    # note: the following maintain order and uniquify the list but only for Python >= 3.7
    to_search = list(dict.fromkeys(
        list(map(lambda x: x.strip(), parcelles_arg.split(';')))).keys())
    env['logger'].debug("verif_parcelle() to_search: %s", to_search)

    # building the WHERE clause of the query
    parcelles_query_conditions = []
    for parcelle in to_search:
        # pylint: disable=no-member
        parcelles_query_conditions.append(func.concat(
            EntityParcelle.code_departement,
            EntityParcelle.code_direction,
            EntityParcelle.code_commune,
            EntityParcelle.code_quartier,
            EntityParcelle.code_section,
            EntityParcelle.code_numero) == parcelle)

    # the result
    parcelles = []

    # "executing" the query
    parcelles_query = (env['db']
                       .query(EntityParcelle)
                       .filter(or_(*parcelles_query_conditions))
                       .order_by(desc(EntityParcelle.id))) # pylint: disable=no-member
    env['logger'].debug("verif_parcelle() query: %s", parcelles_query)

    # for every parcelle matching
    parcelles_found = {}
    for parcelle in parcelles_query:

        # add it to the result
        parcelles_found[parcelle.get_full_code()] = ParcelleReturnExistsType(
            parcelle=parcelle.get_full_code(),
            existe=True,
            adresse=ParcelleAddress(
                numero_voie=parcelle.adresse_num_voie,
                type_voie=parcelle.adresse_typ_voie,
                nom_voie=parcelle.adresse_lib_voie,
                arrondissement=parcelle.code_quartier
                )
        )
    found = list(parcelles_found.keys())
    env['logger'].debug("verif_parcelle() found: %s", found)

    # the list of parcelles not found
    not_found = [p for p in to_search if p not in found]
    env['logger'].debug("verif_parcelle() not found: %s", not_found)

    # re-order the list, starting from the list of parcelle provided
    for parcelle in to_search:

        # parcelle found
        if parcelle in found:
            parcelles.append(parcelles_found[parcelle])

        # parcelle was not found
        else:

            # add it to the result
            parcelles.append(ParcelleReturnNotExistType(
                parcelle=parcelle,
                existe=False
            ))

    # return the result
    env['logger'].debug(
        "verif_parcelle() result: %s", json.dumps(
            {"parcelles": parcelles}, default=lambda o: o.__dict__))
    return {"parcelles": parcelles}


@view_config(route_name="calcul_emprise", renderer="json", request_method="POST", openapi=True)
def calcul_emprise(request: Request) -> t.Dict[str, EmpriseReturnType]: # pylint: disable=too-many-statements,too-many-branches, too-many-locals
    """Trigger and save the 'emprise' calculation of the posted 'parcelles'."""

    # get the environment
    env = get_env_for_data_source_from_request(request, log_msg_prefix='calcul_emprise() ')

    # the result of the emprise calculation
    emprise_calculation = EmpriseReturnType(statut_calcul_emprise=False)

    # numero of the dossier
    numero = request.openapi_validated.parameters['path']['numero_dossier']
    env['logger'].debug("calcul_emprise() numero: %s", numero)

    # search for an existing dossier in openADS and ESRI tables
    dossier_openads, dossier_esri = get_dossier(numero, env['db'], openads=True, esri=True, logger=env['logger'])
    env['logger'].debug("calcul_emprise() dossier openADS: %s", dossier_openads)
    env['logger'].debug("calcul_emprise() dossier ESRI: %s", dossier_esri)

    # if the list of parcelles is empty, it is an error
    if not request.openapi_validated.body.parcelles:
        env['logger'].debug("calcul_emprise() empty parcelles")
        env['logger'].debug(
            "calcul_emprise() result: %s", json.dumps(
                {"emprise": emprise_calculation}, default=lambda o: o.__dict__))
        return {"emprise": emprise_calculation}

    # list of parcelles that will be associated with the created/updated dossier
    parcelles = sorted(list(map(lambda x: x.strip(), request.openapi_validated.body.parcelles)))
    env['logger'].debug("calcul_emprise() parcelles: %s", parcelles)

    # building the WHERE clause of the query
    parcelles_query_conditions = []
    for parcelle in parcelles:
        # pylint: disable=no-member
        parcelles_query_conditions.append(func.concat(
            EntityParcelle.code_departement,
            EntityParcelle.code_direction,
            EntityParcelle.code_commune,
            EntityParcelle.code_quartier,
            EntityParcelle.code_section,
            EntityParcelle.code_numero) == parcelle)

    # get every parcelle instances
    parcelles_query = (env['db']
                       .query(EntityParcelle)
                       .filter(or_(*parcelles_query_conditions))
                       .order_by(desc(EntityParcelle.id))) # pylint: disable=no-member

    # analyse which were found/not found
    parcelles_found = [p.get_full_code() for p in parcelles_query]
    env['logger'].debug("calcul_emprise() parcelles found: %s", parcelles_found)
    parcelles_not_found = [p for p in parcelles if p not in parcelles_found]

    # there is at least one parcelle not found
    if parcelles_not_found:
        env['logger'].debug("calcul_emprise() parcelles not found: %s", parcelles_not_found)

        # if dossier openADS have an emprise value and is not frozen
        if dossier_openads and dossier_openads.geom is not None and not dossier_openads.frozen:

            # remove it
            dossier_openads.geom = None
            env['logger'].debug("calcul_emprise() removed emprise from dossier openADS")

            # save changes
            env['db'].commit()
            env['logger'].debug("calcul_emprise() session commited")

        # if dossier openADS have an emprise value and is frozen
        if dossier_openads and dossier_openads.geom is not None and dossier_openads.frozen:
            emprise_calculation.statut_calcul_emprise = True
            env['logger'].debug("calcul_emprise() dossier openADS (frozen) with an existing emprise")

        # if dossier ESRI have an emprise value
        elif dossier_esri and dossier_esri.geom is not None:

            #  # differences between parcelles lists
            #  parcelles_diff = list(
            #      set(dossier_esri.get_parcelles()).symmetric_difference(set(parcelles)))
            #
            #  # if the list of parcelles are the same
            #  if len(parcelles_diff) < 1:
            #
            #      # fake the calculation (because the emprise has been drawn by a human before)
            #      emprise_calculation.statut_calcul_emprise = True
            #      env['logger'].debug(
            #          "calcul_emprise() dossier ESRI with an existing emprise "
            #          "(parcelles old and new are the same)")
            #
            #  # if the list of parcelles differs: calculation will be considered failed
            #  else:
            #      env['logger'].debug(
            #          "calcul_emprise() dossier ESRI with an existing emprise "
            #          "(parcelles old and new differs: %s)", parcelles_diff)
            #      env['logger'].debug(
            #          "calcul_emprise() impossible emprise calculation. Aborting")

            # consider the emprise as calculated
            # - the emprise has been drawn by a human before
            # - we don't care if the emprise was drawn according to a previous list of parcelles
            #   that might be different now (it's the job of the instructor to update its drawing)
            emprise_calculation.statut_calcul_emprise = True
            env['logger'].debug("calcul_emprise() dossier ESRI with an existing emprise")

        # all other cases: calculation will be considered failed
        else:
            env['logger'].debug("calcul_emprise() impossible emprise calculation. Aborting")

    # all parcelles exists
    else:

        # if dossier ESRI have an emprise value
        if dossier_esri and dossier_esri.geom is not None:

            # remove the dossier ESRI
            env['db'].delete(dossier_esri)
            env['logger'].debug("calcul_emprise() removed dossier ESRI: '%s'", numero)

        # emprise calculation
        # pylint: disable=no-member
        emprise_query = (env['db']
                         .query(func.ST_Union(EntityParcelle.geom).label('geom'),
                                func.ST_AsText(func.ST_Union(EntityParcelle.geom)).label('text'))
                         .filter(or_(*parcelles_query_conditions))
                         .one())

        env['logger'].debug("calcul_emprise() emprise: %s", emprise_query[1])

        # search for an existing dossier
        dossier = get_dossier(numero, env['db'], openads=True, logger=env['logger'])
        env['logger'].debug("calcul_emprise() dossier: %s", dossier)

        # ensure that all parcelles belongs to the same commune
        dossier_commune = None
        parcelles_by_communes = {}
        for parcelle in parcelles_query:
            if parcelle.get_code_insee() not in parcelles_by_communes:
                parcelles_by_communes[parcelle.get_code_insee()] = []
            parcelles_by_communes[parcelle.get_code_insee()].append(parcelle.get_full_code())

        # more than one commune involved
        if len(parcelles_by_communes) > 1:
            env['logger'].warning(
                "dossier '%s' using parcelles from different communes: "
                "%s", numero, parcelles_by_communes)
            commune_max_parcelles = 0
            for commune in parcelles_by_communes:
                if len(commune) > commune_max_parcelles:
                    commune_max_parcelles = len(commune)
                    dossier_commune = commune
            env['logger'].warning(
                "dossier '%s' using commune '%s' with %d parcelles "
                "as default commune", numero, dossier_commune, commune_max_parcelles)

        # no commune at all
        elif len(parcelles_by_communes) < 1:
            env['logger'].warning(
                "dossier '%s' have all parcelles with no commune defined "
                "(parcelles: %s)", numero, parcelles)

        # only one commune
        else:
            dossier_commune = next(iter(parcelles_by_communes))
            env['logger'].debug("calcul_emprise() commune: %s", dossier_commune)

        # new dossier
        if not dossier:
            dossier = EntityDossier()
            dossier.numero = numero
            dossier.parcelles = ','.join(parcelles)
            dossier.code_insee = dossier_commune
            env['db'].add(dossier)
            env['logger'].debug("calcul_emprise() new dossier: %s", dossier)

        # overwrite emprise value
        env['logger'].debug("calcul_emprise() overwrite emprise value: %s%s",
            str(emprise_query[0])[:50], '…' if len(str(emprise_query[0])) > 50 else '')
        dossier.geom = emprise_query[0]

        # save changes
        env['db'].commit()
        env['logger'].debug("calcul_emprise() session commited")

        # emprise was calculated with success
        emprise_calculation.statut_calcul_emprise = True

    env['logger'].debug(
        "calcul_emprise() result: %s", json.dumps(
            {"emprise": emprise_calculation}, default=lambda o: o.__dict__))
    return {"emprise": emprise_calculation}


@view_config(route_name="calcul_centroide", renderer="json", request_method="POST", openapi=True)
def calcul_centroide(request: Request) -> t.Dict[str, CentroideReturnType]:
    """Get the 'centroid' of the posted 'dossier'."""

    # get the environment
    env = get_env_for_data_source_from_request(request, log_msg_prefix='calcul_centroide() ')

    # the result of the centroide calculation: fail by default
    centroide_calculation = CentroideReturnNotExistType(statut_calcul_centroide=False)

    # numero of the dossier
    numero = request.openapi_validated.parameters['path']['numero_dossier']
    env['logger'].debug("calcul_centroide() numero: %s", numero)

    # search for an existing dossier openADS
    dossier = get_dossier(numero, env['db'], openads=True, logger=env['logger'])

    # dossier not found or without an emprise value
    if not dossier or dossier.geom is None:

        # search for an existing dossier ESRI
        dossier = get_dossier(numero, env['db'], esri=True, logger=env['logger'])

    env['logger'].debug("calcul_centroide() dossier: %s", dossier)

    # a dossier was found with an emprise value
    if dossier and dossier.geom is not None:

        # calculate the centroide
        centroide = get_centroide(dossier, env['db'], logger=env['logger'])

        # return the result of the centroide calculation
        centroide_calculation = CentroideReturnExistsType(statut_calcul_centroide=True,
                                                          coord_x=centroide[0],
                                                          coord_y=centroide[1])
    env['logger'].debug(
        "calcul_centroide() result: %s", json.dumps(
            {"centroide": centroide_calculation}, default=lambda o: o.__dict__))
    return {"centroide": centroide_calculation}


@view_config(route_name="contraintes_dossier", renderer="json", request_method="GET", openapi=True)
def recup_contrainte_dossier(request: Request) -> t.Dict[str, t.List[ContrainteReturnType]]:
    """Get the 'contraintes' of the dossier."""

    # get the environment
    env = get_env_for_data_source_from_request(request, log_msg_prefix='recup_contrainte_dossier() ')

    # matching contraintes
    contraintes = []

    # numero of the dossier
    numero = request.openapi_validated.parameters['path']['numero_dossier']
    env['logger'].debug("recup_contrainte_dossier() numero: %s", numero)

    # search for an existing dossier openADS
    dossier = get_dossier(numero, env['db'], openads=True, logger=env['logger'])

    # dossier not found or without an emprise value
    if not dossier or dossier.geom is None:

        # search for an existing dossier ESRI
        dossier = get_dossier(numero, env['db'], esri=True, logger=env['logger'])

    env['logger'].debug("recup_contrainte_dossier() dossier: %s", dossier)

    # a dossier was found with an emprise value
    if dossier and dossier.geom is not None:

        # get its contraintes matching its emprise
        # pylint: disable=no-member
        contraintes_conditions = and_(
            EntityContrainte.code_insee == dossier.code_insee,
            func.ST_Intersects(EntityContrainte.geom, dossier.geom))
        contraintes_query = (env['db']
                             .query(EntityContrainte)
                             .filter(contraintes_conditions)
                             .order_by(desc(EntityContrainte.id)))
        env['logger'].debug("recup_contrainte_dossier() query: %s", contraintes_query)
        for contrainte in contraintes_query:
            contraintes.append(ContrainteReturnType(
                contrainte=contrainte.id_openads,
                groupe_contrainte=contrainte.groupe.strip(),
                sous_groupe_contrainte=contrainte.sous_groupe.strip(),
                libelle=contrainte.libelle,
                texte=contrainte.get_texte()))

    # no dossier found or withtout an emprise value
    else:
        env['logger'].warning(
            "recup_contrainte_dossier() Failed to get dossier: %s "
            "(or no emprise defined)", numero)

    env['logger'].debug(
        "recup_contrainte_dossier() result: %s", json.dumps(
            {"contraintes": ('.. %d contrainte%s ..' % (
                len(contraintes), 's' if len(contraintes) > 1 else ''))},
            default=lambda o: o.__dict__))
    return {"contraintes": contraintes}


@view_config(route_name="contraintes_commune", renderer="json", request_method="GET", openapi=True)
def recup_toutes_contraintes(request: Request) -> t.Dict[str, t.List[ContrainteReturnType]]:
    """Get all 'contraintes' of the 'commune'."""

    # get the environment
    env = get_env_for_data_source_from_request(request, log_msg_prefix='recup_toutes_contraintes() ')

    # matching contraintes
    contraintes = []

    # commune specified
    commune = request.openapi_validated.parameters['path']['code_insee']
    env['logger'].debug("recup_toutes_contraintes() commune: %s", commune)

    # get its contraintes matching its emprise
    contraintes_query = (env['db']
                         .query(EntityContrainte)
                         .filter_by(code_insee=commune)
                         .distinct(EntityContrainte.id) # pylint: disable=no-member
                         .order_by(desc(EntityContrainte.id))) # pylint: disable=no-member
    env['logger'].debug("recup_toutes_contraintes() query: %s", contraintes_query)
    for contrainte in contraintes_query.all():
        contraintes.append(ContrainteReturnType(
            contrainte=contrainte.id_openads,
            groupe_contrainte=contrainte.groupe.strip(),
            sous_groupe_contrainte=contrainte.sous_groupe.strip(),
            libelle=contrainte.libelle,
            texte=(contrainte.text_generic if contrainte.text_generic else '')))

    env['logger'].debug(
        "recup_toutes_contraintes() result: %s", json.dumps(
            {"contraintes": ('.. %d contrainte%s ..' % (
                len(contraintes), 's' if len(contraintes) > 1 else ''))},
            default=lambda o: o.__dict__))
    return {"contraintes": contraintes}


@notfound_view_config()
def resource_not_found_error(context: HTTPNotFound, request: Request) -> exception_response: # pylint: disable=unused-argument
    """If a resource is not found, return error as response."""
    errors = [extract_error(context)]
    return exception_response(404, json_body=errors)


@exception_view_config(RequestValidationError, renderer="json")
def openapi_validation_error(context: HTTPException, request: Request) -> exception_response: # pylint: disable=unused-argument
    """If there are errors when handling the request, return them as response."""
    errors = [extract_error(err) for err in context.errors]
    return exception_response(400, json_body=errors)


@exception_view_config(DatabaseError, renderer="json")
def database_error(context: DatabaseError, request: Request) -> exception_response:
    """If there is a database error, catch it, do a rollback and respond with 500 error."""

    # get the environment
    env = get_env_for_data_source_from_request(request, log_msg_prefix='database_error() ')

    # log the exception (context object)
    env['logger'].debug("database_error() exception: %s", context.__class__.__name__)
    env['logger'].debug("database_error() context.args: %s", context.args)
    env['logger'].debug("database_error() context.statement: %s", context.statement)
    env['logger'].debug("database_error() context.params: %s", context.params)
    env['logger'].debug("database_error() context.orig: %s", context.orig)
    env['logger'].debug("database_error() context.detail: %s", context.detail)

    # get the error message
    text = '(DatabaseError)'
    if context.args and len(context.args):
        text = str(context.args[0]).replace('\n', ' ')
    elif context.orig and len(context.orig):
        text += ' ' + str(context.orig)

    # rollback the transaction
    env['db'].rollback()
    text += ' (transaction was rolled back)'

    # build and return the response
    env['logger'].debug("database_error() text: %s", text)
    output = {"message": text, "exception": context.__class__.__name__}
    errors = [output]
    return exception_response(500, json_body=errors)


def extract_error(err: OpenAPIError, field_name: str = None) -> t.Dict[str, str]:
    """Extract error JSON response using an Exception instance."""
    output = {"message": str(err), "exception": err.__class__.__name__}

    if getattr(err, "name", None) is not None:
        field_name = err.name
    if getattr(err, "property_name", None) is not None:
        field_name = err.property_name
    if field_name is None:
        if isinstance(getattr(err, "original_exception", None), OpenAPIMappingError):
            return extract_error(err.original_exception, field_name)
    if field_name is not None:
        output.update({"field": field_name})
    return output


def app(api_file):
    """Prepare a Pyramid app."""
    global LOGGER

    with Configurator() as config:
        config.include("pyramid_openapi3")
        LOGGER.debug("Using API spec file: '%s'", api_file)
        config.pyramid_openapi3_spec(api_file)
        config.pyramid_openapi3_add_explorer()
        config.add_route("verif_parcelle", "/parcelles/{parcelles}")
        config.add_route("calcul_emprise", "/dossiers/{numero_dossier}/emprise")
        config.add_route("calcul_centroide", "/dossiers/{numero_dossier}/centroide")
        config.add_route("contraintes_dossier", "/dossiers/{numero_dossier}/contraintes")
        config.add_route("contraintes_commune", "/communes/{code_insee}/contraintes")
        config.scan(".")
        return config.make_wsgi_app()


# program called from command line
if __name__ == "__main__":

    # define command line arguments and options
    import argparse
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        '-c', '--config', help="Path to the configuration file",
        default=CONFIG_FILE_PATH)
    arg_parser.add_argument(
        '-a', '--openapi', help="Path to the open API schema file",
        default=OPENAPI_FILE_PATH)
    args = arg_parser.parse_args()

    # check arguments
    if not os.path.isfile(args.config):
        fatal_error("configuration file '%s' doesn't exist\n" % args.config)
    if not os.path.isfile(args.openapi):
        fatal_error("open API specs file '%s' doesn't exist\n" % args.openapi)

    # load configuration
    config = configparser.ConfigParser()
    config.read(args.config)
    try:
        config_defaults = config['APPLICATION']
    except KeyError as exception:
        fatal_error("configuration file '%s' do not have a '%s' section" % (
            args.config, str(exception)))
    try:
        DEFAULT_ENV = config_defaults['env_default']
    except KeyError as exception:
        fatal_error("configuration file '%s' do not have required key '%s'" % (
            args.config, str(exception)))

    # load environments
    load_environements(config)
    if not DEFAULT_ENV in ENVIRONMENTS:
        fatal_error("default environment '%s' doesn't exist" % DEFAULT_ENV)

    # setup default logger
    setup_default_logger(config)
    LOGGER.debug("BEGIN")

    # print database tables for each environments
    # (a way to test the connection and be sure of the schema conformance)
    for env_name, env in ENVIRONMENTS.items():
        env['logger'].debug("BEGIN")
        try:
            env['logger'].debug("Tables: %s", sorted(list(filter(lambda x: not re.search(r'^i[0-9]+$',
                x), env['db-engine'].table_names()))))
        except OperationalError as exception:
            fatal_error("database error for environment '%s': %s" % (
                env_name, str(exception)))

    # list enabled environment
    LOGGER.debug("Default env : %s", DEFAULT_ENV)
    LOGGER.debug("Environments: %s", ENVIRONMENTS)

    # If app.py is called directly, start up the app
    host = config['APPLICATION']['app_host']
    port = int(config['APPLICATION']['app_port'])
    LOGGER.info("Swagger UI available at %s", 'http://{}:{}/docs/'.format(host, port))  # noqa: T001
    SERVER = make_server(host, port, app(args.openapi))
    SERVER.serve_forever()
