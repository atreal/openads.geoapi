openapi: "3.0.0"

info:
  version: "1.0.0"
  title: openADS.geoAPI middleware between openADS and any SIG without an API
  contact:
    name: "atReal support"
    email: "support+openads.geoapi@atreal.fr"
  license:
    name: "GPLv3"
    url: "https://www.gnu.org/licenses/gpl-3.0.txt"

servers:
  - url: "http://127.0.0.1:6543/"
    description: "Development server"

paths:

  /parcelles/{parcelles}:

    get:

      summary: Check 'parcelles' existence and return their addresses

      parameters:
        - name: parcelles
          in: path
          description: "A list of parcelle identifiers separated by semi-colon"
          required: true
          schema:
            type: string
            pattern: '^\w+(;\w+)*$'
        - name: datasrc
          in: query
          description: "The data source to use"
          required: false
          schema:
            type: string
            pattern: '^\w+$'

      responses:
        '500':
          $ref: '#/components/responses/InternalServerError'

        '400':
          $ref: '#/components/responses/BadRequest'

        '404':
          $ref: '#/components/responses/InvalidDataSource'

        '200':
          description: A list of parcelles info/checks and addresses
          content:
            application/json:
              schema:
                type: object
                properties:
                  parcelles:
                    type: array
                    items:
                      anyOf:
                        - $ref: "#/components/schemas/ParcelleReturnExists"
                        - $ref: "#/components/schemas/ParcelleReturnNotExist"
                required:
                  - parcelles
              examples:
                existing:
                  summary: a sample response with an existing 'parcelle'
                  value:
                    parcelles:
                      - parcelle: "1312158980H0126"
                        existe: false
                        adresse:
                          numero_voie": "666"
                          type_voie": "RUE"
                          nom_voie": "DE LA LIBERTE"
                          arrondissement": "11"
                not-existing:
                  summary: a sample response with no 'parcelle' existing
                  value:
                    parcelles:
                      - parcelle: "1312158980H0126"
                        existe: false


  /dossiers/{numero_dossier}/emprise:

    post:

      summary: Trigger and save the 'emprise' calculation of the posted 'parcelles'

      parameters:
        - name: numero_dossier
          in: path
          description: "The 'numero dossier' identifier"
          required: true
          schema:
            type: string
            pattern: '^\w+'
        - name: datasrc
          in: query
          description: "The data source to use"
          required: false
          schema:
            type: string
            pattern: '^\w+$'

      requestBody:
        required: true
        description: The list of 'parcelles' identifier
        content:
          application/json:
            schema:
              type: object
              properties:
                parcelles:
                  type: array
                  items:
                    $ref: '#/components/schemas/ParcelleIdentifier'
              required:
                - parcelles
            examples:
              parcelles:
                summary: a sample request body
                value:
                  parcelles:
                    - "1312158980H0126"
                    - "1312158980H0127"

      responses:
        '500':
          $ref: '#/components/responses/InternalServerError'

        '400':
          $ref: '#/components/responses/BadRequest'

        '404':
          $ref: '#/components/responses/InvalidDataSource'

        '200':
          description: Result of the 'emprise' calculation
          content:
            application/json:
              schema:
                type: object
                properties:
                  emprise:
                    type: object
                    properties:
                      statut_calcul_emprise:
                        type: boolean
                    required:
                      - statut_calcul_emprise
                required:
                  - emprise
              examples:
                calculation-success:
                  summary: a successful calculation response
                  value:
                    emprise:
                      statut_calcul_emprise: true
                calculation-failed:
                  summary: a failed calculation response
                  value:
                    emprise:
                      statut_calcul_emprise: false


  /dossiers/{numero_dossier}/centroide:

    post:

      summary: Get the 'centroid' of the posted 'parcelles'

      parameters:
        - name: numero_dossier
          in: path
          description: "The 'numero dossier' identifier"
          required: true
          schema:
            type: string
            pattern: '^\w+'
        - name: datasrc
          in: query
          description: "The data source to use"
          required: false
          schema:
            type: string
            pattern: '^\w+$'

      responses:
        '500':
          $ref: '#/components/responses/InternalServerError'

        '400':
          $ref: '#/components/responses/BadRequest'

        '404':
          $ref: '#/components/responses/InvalidDataSource'

        '200':
          description: Result of the 'centroide' calculation
          content:
            application/json:
              schema:
                type: object
                properties:
                  emprise:
                    oneOf:
                      - type: object
                        properties:
                          statut_calcul_centroide:
                            type: boolean
                            pattern: '^false$'
                        required:
                          - statut_calcul_centroide
                      - type: object
                        properties:
                          statut_calcul_centroide:
                            type: boolean
                            pattern: '^true$'
                          x:
                            type: number
                          y:
                            type: number
                        required:
                          - statut_calcul_centroide
                          - x
                          - y
                required:
                  - centroide
              examples:
                calculation-success:
                  summary: a successful calculation response
                  value:
                    centroide:
                      statut_calcul_centroide: true
                      x: "1888778.84"
                      y: "3131268.88"
                calculation-failed:
                  summary: a failed calculation response
                  value:
                    centroide:
                      statut_calcul_centroide: false


  /dossiers/{numero_dossier}/contraintes:

    get:

      summary: Get the 'contraintes' of the dossier

      parameters:
        - name: numero_dossier
          in: path
          description: "The 'numero dossier' identifier"
          required: true
          schema:
            type: string
            pattern: '^\w+'
        - name: datasrc
          in: query
          description: "The data source to use"
          required: false
          schema:
            type: string
            pattern: '^\w+$'

      responses:
        '500':
          $ref: '#/components/responses/InternalServerError'

        '400':
          $ref: '#/components/responses/BadRequest'

        '404':
          $ref: '#/components/responses/InvalidDataSource'

        '200':
          description: A list of contraintes
          content:
            application/json:
              schema:
                type: object
                properties:
                  contraintes:
                    type: array
                    items:
                      $ref: "#/components/schemas/ContrainteReturnExists"
                required:
                  - contraintes
              examples:
                existing:
                  summary: a sample response when 'contraintes' exists
                  value:
                    contraintes:
                      - contrainte: "26"
                        groupe_contrainte: "Zonage"
                        sous_groupe_contrainte: "Projet"
                        libelle: "1AUba"
                        texte: "Zone d'urbanisation à moyen terme"
                      - contrainte: "14"
                        groupe_contrainte: "Zonage"
                        sous_groupe_contrainte: "Environnement"
                        libelle: "Prox. ZUS 500m"
                        texte: "Proximité d'une ZUS à moins de 500m"
                        # [...]
                not-existing:
                  summary: a sample response when no 'contraintes' exists
                  value:
                    contraintes: []


  /communes/{code_insee}/contraintes:

    get:

      summary: Get all 'contraintes' of the 'commune'

      parameters:
        - name: code_insee
          in: path
          description: "The INSSE code of the 'commune'"
          required: true
          schema:
            type: string
            pattern: '^\d{5}$'
        - name: datasrc
          in: query
          description: "The data source to use"
          required: false
          schema:
            type: string
            pattern: '^\w+$'

      responses:
        '500':
          $ref: '#/components/responses/InternalServerError'

        '400':
          $ref: '#/components/responses/BadRequest'

        '404':
          $ref: '#/components/responses/InvalidDataSource'

        '200':
          description: A list of contraintes
          content:
            application/json:
              schema:
                type: object
                properties:
                  contraintes:
                    type: array
                    items:
                      $ref: "#/components/schemas/ContrainteReturnExists"
                required:
                  - contraintes
              examples:
                existing:
                  summary: a sample response when 'contraintes' exists
                  value:
                    contraintes:
                      - contrainte: "26"
                        groupe_contrainte: "Zonage"
                        sous_groupe_contrainte: "Projet"
                        libelle: "1AUba"
                        texte: "Zone d'urbanisation à moyen terme"
                      - contrainte: "14"
                        groupe_contrainte: "Zonage"
                        sous_groupe_contrainte: "Environnement"
                        libelle: "Prox. ZUS 500m"
                        texte: "Proximité d'une ZUS à moins de 500m"
                        # [...]
                not-existing:
                  summary: a sample response when no 'contraintes' exists
                  value:
                    contraintes: []


components:

  schemas:

    ParcelleIdentifier:
      type: string
      maxLength: 15

    ParcelleAddress:
      type: object
      properties:
        numero_voie:
          type: string
          maxLength: 10
        type_voie:
          type: string
          maxLength: 50
        nom_voie:
          type: string
          maxLength: 150
        arrondissement:
          type: string
          maxLength: 2
      required:
        - numero_voie
        - type_voie
        - nom_voie

    ParcelleReturnExists:
      type: object
      properties:
        parcelle:
          $ref: '#/components/schemas/ParcelleIdentifier'
        existe:
          type: boolean
          enum: [true]
        adresse:
          $ref: '#/components/schemas/ParcelleAddress'
      required:
        - parcelle
        - existe
        - adresse

    ParcelleReturnNotExist:
      type: object
      properties:
        parcelle:
          $ref: '#/components/schemas/ParcelleIdentifier'
        existe:
          type: boolean
          enum: [false]
      required:
        - parcelle
        - existe


    ContrainteIdentifier:
      type: string
      maxLength: 10

    ContrainteReturnExists:
      type: object
      properties:
        contrainte:
          $ref: '#/components/schemas/ContrainteIdentifier'
        groupe_contrainte:
          type: string
          maxLength: 250
        sous_groupe_contrainte:
          type: string
          maxLength: 250
        libelle:
          type: string
          maxLength: 250
        texte:
          type: string
      required:
        - contrainte
        - groupe_contrainte
        - sous_groupe_contrainte
        - libelle
        - texte


    Error:
      type: object
      required:
        - message
      properties:
        field:
          type: string
        message:
          type: string
        exception:
          type: string


  responses:

    InternalServerError:
      description: Unknown error while processing request
      content:
        application/json:
          schema:
            type: array
            items:
              $ref: "#/components/schemas/Error"
        text/plain:
          schema:
            type: string

    BadRequest:
      description: OpenAPI request/response validation failed
      content:
        application/json:
          schema:
            type: array
            items:
              $ref: "#/components/schemas/Error"

    InvalidDataSource:
      description: Invalid data source
      content:
        application/json:
          schema:
            type: array
            items:
              $ref: "#/components/schemas/Error"
