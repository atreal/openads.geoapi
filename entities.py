"""Entities definitions."""


class EntityParcelle(): # pylint: disable=too-many-instance-attributes
    """Entity 'Parcelle' object class"""

    # pylint: disable=too-many-arguments
    def __init__(self, _id=None, code_commune=None, code_departement=None, code_direction=None,
                 code_quartier=None, code_section=None, code_numero=None, adresse_num_voie=None,
                 adresse_ind_voie=None, adresse_typ_voie=None, adresse_lib_voie=None, geom=None):
        self.id = _id  # pylint: disable=invalid-name
        self.code_commune = code_commune
        self.code_departement = code_departement
        self.code_direction = code_direction
        self.code_quartier = code_quartier
        self.code_section = code_section
        self.code_numero = code_numero
        self.adresse_num_voie = adresse_num_voie
        self.adresse_ind_voie = adresse_ind_voie
        self.adresse_typ_voie = adresse_typ_voie
        self.adresse_lib_voie = adresse_lib_voie
        self.geom = geom

    def get_code_insee(self):
        """Return the parcelle's INSEE code based on departement and commune codes."""
        return u'%s%s' % (self.code_departement, self.code_commune)

    def get_full_code(self):
        """Return the full parcelle's code based on all codes."""
        return u'%s%s%s%s%s%s' % (
            self.code_departement, self.code_direction, self.code_commune,
            self.code_quartier, self.code_section, self.code_numero)

    def get_adresse(self):
        """Return the parcelle's adresse based on all adresse attributes."""
        return u'%s%s%s%s' % (
            str(int(self.adresse_num_voie)) + ' ' if self.adresse_num_voie else '',
            self.adresse_ind_voie + ' ' if self.adresse_ind_voie else '',
            self.adresse_typ_voie + ' ' if self.adresse_typ_voie else '',
            self.adresse_lib_voie)

    def __repr__(self):
        return "<Parcelle(id='%d', code='%s')>" % (self.id, self.get_full_code())


class EntityContrainte(): # pylint: disable=too-many-instance-attributes
    """Entity 'Contrainte' object class"""

    # pylint: disable=too-many-arguments
    def __init__(self, _id=None, id_openads=None, id_contrainte=None, text_generic=None,
                 text_specific=None, libelle=None, groupe=None, sous_groupe=None, code_insee=None,
                 geom=None):
        self.id = _id  # pylint: disable=invalid-name
        self.id_openads = id_openads
        self.id_contrainte = id_contrainte
        self.text_generic = text_generic
        self.text_specific = text_specific
        self.libelle = libelle
        self.groupe = groupe
        self.sous_groupe = sous_groupe
        self.code_insee = code_insee
        self.geom = geom

    def get_texte(self):
        """Return the contrainte texte."""
        return self.text_specific if self.text_specific else self.text_generic

    def __repr__(self):
        return ("<Contrainte(id_openads='%d', id_contrainte='%s', text='%s', grp='%s', sgrp='%s', "
                "insee='%s')>" % (
                    self.id_openads, self.id_contrainte,
                    self.get_texte(),
                    self.groupe.strip(), self.sous_groupe.strip(),
                    self.code_insee))


class EntityDossier():
    """Entity 'Dossier' object class."""

    def __init__(self, _id=None, numero=None, code_insee=None, parcelles=None, geom=None,
            frozen=False):
        self.id = _id
        self.numero = numero
        self.code_insee = code_insee
        self.parcelles = parcelles
        self.geom = geom
        self.frozen = frozen

    def get_parcelles(self):
        """Return the list of parcelles."""
        if not self.parcelles:
            return []
        return sorted(list(filter(len, map(lambda x: x.strip(), self.parcelles.split(',')))))

    def __repr__(self):
        return "<Dossier(id='%s', numero='%s', insee='%s', parcelles='%s', emprise='%s')>" % (
            self.id, self.numero, self.code_insee,
            ','.join(self.get_parcelles()), self.geom is not None)


class EntityDossierESRI(): # pylint: disable=too-few-public-methods
    """Entity 'DossierESRI' object class."""

    def __init__(self, _id=None, numero=None, code_insee=None, geom=None):
        self.id = _id
        self.numero = numero
        self.code_insee = code_insee
        self.geom = geom

    def __repr__(self):
        return "<DossierESRI(id='%s', numero='%s', insee='%s', emprise='%s')>" % (
            self.id, self.numero, self.code_insee, self.geom is not None)
